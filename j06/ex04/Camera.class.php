<?php

class Camera {

	public static $verbose = False;
	private $_ratio;
	private $_width;
	private $_height;
	private $_fov;
	private $_near;
	private $_far;
	private $_origin;
	private $_tT;
	private $_tR;
	private $_viewM;
	private $_proj;

	function __construct(array $kwargs) {
		$this->_origin = $kwargs['origin'];
		$this->_tR = $kwargs['orientation']->transpos();
		if (array_key_exists('ratio', $kwargs))
			$this->_ratio = $kwargs['ratio'];
		else {
			$this->_width = $kwargs['width'];
			$this->_height = $kwargs['height'];
			$this->_ratio = $this->_width / $this->_height;
		}
		$this->_fov = $kwargs['fov'];
		$this->_near = $kwargs['near'];
		$this->_far = $kwargs['far'];
		$vtc = new Vector(array('dest' => $this->_origin));
		$this->_tT = new Matrix(array('preset' => Matrix::TRANSLATION, 'vtc' => $vtc->opposite()));
		$this->_viewM = $this->_tR->mult($this->_tT);
		$this->_proj = new Matrix(array('preset' => Matrix::PROJECTION, 'fov' => $this->_fov, 'ratio' => $this->_ratio, 'near' => $this->_near, 'far' => $this->_far));
		if (self::$verbose == True)
			print('Camera instance constructed'.PHP_EOL);
	}

	function __destruct() {
		if (self::$verbose == True)
			print('Camera instance destructed'.PHP_EOL);
	}

	function __toString() {
		return 'Camera('.PHP_EOL.'+ Origine: '.$this->_origin.PHP_EOL.'+ tT:'.PHP_EOL.$this->_tT.PHP_EOL.'+ tR:'.PHP_EOL.$this->_tR.PHP_EOL.'+ tR->mult( tT ):'.$this->_viewM.PHP_EOL.'+ Proj:'.PHP_EOL.$this->_proj.PHP_EOL.')';
	}

	function watchVertex(Vertex $worldVertex) {
		$x = (($worldVertex->getX() + 1) / 2) * $this->_width;
		$y = ((1 - $worldVertex->getY()) / 2) * $this->_height;
		$z = $worldVertex->getZ();
		return new Vertex(array('x' => $x, 'y' => $y, array('z') => $z));
	}

}

?>