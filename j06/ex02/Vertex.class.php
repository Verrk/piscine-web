<?php

require_once 'Color.class.php';

class Vertex {

	private $_x;
	private $_y;
	private $_z;
	private $_w = 1.0;
	private $_color;
	public static $verbose = False;

	function __construct(array $kwargs) {
		$this->_x = $kwargs['x'];
		$this->_y = $kwargs['y'];
		$this->_z = $kwargs['z'];
		if (array_key_exists('w', $kwargs))
			$this->_w = $kwargs['w'];
		if (array_key_exists('color', $kwargs))
			$this->_color = $kwargs['color'];
		else
			$this->_color = new Color(array('rgb' => 0xffffff));
		if (self::$verbose == True) {
			print('Vertex( x: '.number_format($this->getX(), 2).', y: '.number_format($this->getY(), 2).', z:'.number_format($this->getZ(), 2).', w:'.number_format($this->getW(), 2).', '.$this->getColor().' ) constructed'.PHP_EOL);
		}
	}

	function __destruct() {
		if (self::$verbose == True)
			print('Vertex( x: '.number_format($this->getX(), 2).', y: '.number_format($this->getY(), 2).', z:'.number_format($this->getZ(), 2).', w:'.number_format($this->getW(), 2).', '.$this->getColor().' ) destructed'.PHP_EOL);
	}

	function getX() {
		return $this->_x;
	}

	function setX($x) {
		$this->_x = $x;
	}

	function getY() {
		return $this->_y;
	}

	function setY($y) {
		$this->_y = $y;
	}

	function getZ() {
		return $this->_z;
	}

	function setZ($z) {
		$this->_z = $z;
	}

	function getW() {
		return $this->_w;
	}

	function setW($w) {
		$this->_w = $w;
	}

	function getColor() {
		return $this->_color;
	}

	function setColor($color) {
		$this->_color->setRed($color->getRed);
		$this->_color->setGreen($color->getGreen);
		$this->_color->setBlue($color->getBlue);
	}

	function __toString() {
		$str = 'Vertex( x: '.number_format($this->getX(), 2).', y: '.number_format($this->getY(), 2).', z:'.number_format($this->getZ(), 2).', w:'.number_format($this->getW(), 2);
		if (self::$verbose == True)
			$str = $str.', '.$this->getColor();
		$str = $str.' )';
		return $str;
	}

}

?>