<?php

class Color {

	public static $verbose = False;
	private $_red = 0;
	private $_green = 0;
	private $_blue = 0;

	function __construct(array $kwargs) {
		if (array_key_exists('rgb', $kwargs)) {
			$this->_red = ($kwargs['rgb'] & (255 << 16)) / 65536;
			$this->_green = ($kwargs['rgb'] & (255 << 8)) / 256;
			$this->_blue = $kwargs['rgb'] & 255;
		}
		else {
			$this->_red = (int)$kwargs['red'];
			$this->_green = (int)$kwargs['green'];
			$this->_blue = (int)$kwargs['blue'];
		}
		if (self::$verbose == True) {
			print('Color( red: '.$this->printColor($this->_red).', green: '.$this->printColor($this->_green).', blue: '.$this->printColor($this->_blue).' ) constructed.'.PHP_EOL);
		}
	}

	function __destruct() {
		if (self::$verbose == TRUE) {
			print('Color( red: '.$this->printColor($this->_red).', green: '.$this->printColor($this->_green).', blue: '.$this->printColor($this->_blue).' ) destructed.'.PHP_EOL);
		}
	}

	function getRed() {
		return $this->_red;
	}

	function setRed($red) {
		$this->_red = $red;
	}

	function getGreen() {
		return $this->_green;
	}

	function setGreen($green) {
		$this->_green = $green;
	}

	function getBlue() {
		return $this->_blue;
	}

	function setBlue($blue) {
		$this->_blue = $blue;
	}

	private function printColor($color) {
		$str = '';
		if ($color < 10)
			$str = '  ';
		else if ($color < 100)
			$str = ' ';
		$str = $str.$color;
		return $str;
	}

	function __toString() {
		return 'Color( red: '.$this->printColor($this->_red).', green: '.$this->printColor($this->_green).', blue: '.$this->printColor($this->_blue).' )';
	}

	function add(Color $c) {
		return new Color(array('red' => $this->_red + $c->_red, 'green' => $this->_green + $c->_green, 'blue' => $this->_blue + $c->_blue));
	}

	function sub(Color $c) {
		return new Color(array('red' => $this->_red - $c->_red, 'green' => $this->_green - $c->_green, 'blue' => $this->_blue - $c->_blue));
	}

	function mult($f) {
		return new Color(array('red' => $this->_red * $f, 'green' => $this->_green * $f, 'blue' => $this->_blue * $f));
	}

}

?>