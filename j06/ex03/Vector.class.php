<?php

class Vector {
	private $_x;
	private $_y;
	private $_z;
	private $_w = 0;
	public static $verbose = False;

	function __construct(array $kwargs) {
		$destX = $kwargs['dest']->getX();
		$destY = $kwargs['dest']->getY();
		$destZ = $kwargs['dest']->getZ();
		if (array_key_exists('orig', $kwargs)) {
			$origX = $kwargs['orig']->getX();
			$origY = $kwargs['orig']->getY();
			$origZ = $kwargs['orig']->getZ();
		}
		else {
			$v = new Vertex(array('x' => 0, 'y' => 0, 'z' => 0, 'w' => 1));
			$origX = $v->getX();
			$origY = $v->getY();
			$origZ = $v->getZ();
		}
		$this->_x = $destX - $origX;
		$this->_y = $destY - $origY;
		$this->_z = $destZ - $origZ;
		if (self::$verbose == True) {
			print('Vector( x:'.number_format($this->getX(), 2).', y:'.number_format($this->getY(), 2).', z:'.number_format($this->getZ(), 2).', w:'.number_format($this->getW(), 2).' ) constructed'.PHP_EOL);
		}
	}

	function __destruct() {
		if (self::$verbose == True) {
			print('Vector( x:'.number_format($this->getX(), 2).', y:'.number_format($this->getY(), 2).', z:'.number_format($this->getZ(), 2).', w:'.number_format($this->getW(), 2).' ) destructed'.PHP_EOL);
		}
	}

	function getX() {
		return $this->_x;
	}

	function getY() {
		return $this->_y;
	}

	function getZ() {
		return $this->_z;
	}

	function getW() {
		return $this->_w;
	}

	function __toString() {
		return 'Vector( x:'.number_format($this->getX(), 2).', y:'.number_format($this->getY(), 2).', z:'.number_format($this->getZ(), 2).', w:'.number_format($this->getW(), 2).' )';
	}

	function magnitude() {
		return sqrt($this->getX() ** 2 + $this->getY() ** 2 + $this->getZ() ** 2);
	}

	function normalize() {
		$mag = $this->magnitude();
		$x = $this->getX() / $mag;
		$y = $this->getY() / $mag;
		$z = $this->getZ() / $mag;
		$nVtx = new Vertex(array('x' => $x, 'y' => $y, 'z' => $z));
		return new Vector(array('dest' => $nVtx));
	}

	function add(Vector $rhs) {
		$x = $this->getX() + $rhs->getX();
		$y = $this->getY() + $rhs->getY();
		$z = $this->getZ() + $rhs->getZ();
		$vtx = new Vertex(array('x' => $x, 'y' => $y, 'z' => $z));
		return new Vector(array('dest' => $vtx));
	}

	function sub(Vector $rhs) {
		$x = $this->getX() - $rhs->getX();
		$y = $this->getY() - $rhs->getY();
		$z = $this->getZ() - $rhs->getZ();
		$vtx = new Vertex(array('x' => $x, 'y' => $y, 'z' => $z));
		return new Vector(array('dest' => $vtx));
	}

	function opposite() {
		$x = -$this->getX();
		$y = -$this->getY();
		$z = -$this->getZ();
		$vtx = new Vertex(array('x' => $x, 'y' => $y, 'z' => $z));
		return new Vector(array('dest' => $vtx));
	}

	function scalarProduct($k) {
		$x = $this->getX() * $k;
		$y = $this->getY() * $k;
		$z = $this->getZ() * $k;
		$vtx = new Vertex(array('x' => $x, 'y' => $y, 'z' => $z));
		return new Vector(array('dest' => $vtx));
	}

	function dotProduct(Vector $rhs) {
		return $this->getX() * $rhs->getX() +
		$this->getY() * $rhs->getY() +
		$this->getZ() * $rhs->getZ();
	}

	function crossProduct(Vector $rhs) {
		$x = $this->getY() * $rhs->getZ() - $this->getZ() * $rhs->getY();
		$y = $this->getZ() * $rhs->getX() - $this->getX() * $rhs->getZ();
		$z = $this->getX() * $rhs->getY() - $this->getY() * $rhs->getX();
		$vtx = new Vertex(array('x' => $x, 'y' => $y, 'z' => $z));
		return new Vector(array('dest' => $vtx));
	}

	function cos(Vector $rhs) {
		return $this->dotProduct($rhs) / ($this->magnitude() * $rhs->magnitude());
	}

}

?>