<?php

class Matrix {
	const IDENTITY = 1;
	const SCALE = 2;
	const RX = 3;
	const RY = 4;
	const RZ = 5;
	const TRANSLATION = 6;
	const PROJECTION = 7;

	private $_matrix = array(array(0, 0, 0, 0), array(0, 0, 0, 0), array(0, 0, 0, 0), array(0, 0, 0, 0));
	public static $verbose = False;

	function __construct(array $kwargs) {
		if (array_key_exists('preset', $kwargs)) {
			if ($kwargs['preset'] == self::IDENTITY) {
				$str = 'IDENTITY';
				$this->_matrix = array(array(1, 0, 0, 0), array(0, 1, 0, 0), array(0, 0, 1, 0), array(0, 0, 0, 1));
			}
			if ($kwargs['preset'] == self::TRANSLATION) {
				$str = 'TRANSLATION preset';
				$vtc = $kwargs['vtc'];
				$this->_matrix = array(array(1, 0, 0, $vtc->getX()), array(0, 1, 0, $vtc->getY()), array(0, 0, 1, $vtc->getZ()), array(0, 0, 0, 1));
			}
			if ($kwargs['preset'] == self::SCALE) {
				$str = 'SCALE preset';
				$s = $kwargs['scale'];
				$this->_matrix = array(array($s, 0, 0, 0), array(0, $s, 0, 0), array(0, 0, $s, 0), array(0, 0, 0, 1));
			}
			if ($kwargs['preset'] == self::RX) {
				$str = 'Ox ROtATION preset';
				$o = $kwargs['angle'];
				$this->_matrix = array(array(1, 0, 0, 0), array(0, cos($o), -sin($o), 0), array(0, sin($o), cos($o), 0), array(0, 0, 0, 1));
			}
			if ($kwargs['preset'] == self::RY) {
				$str = 'Oy ROtATION preset';
				$o = $kwargs['angle'];
				$this->_matrix = array(array(cos($o), 0, sin($o), 0), array(0, 1, 0, 0), array(-sin($o), 0, cos($o), 0), array(0, 0, 0, 1));
			}
			if ($kwargs['preset'] == self::RZ) {
				$str = 'Oz ROtATION preset';
				$o = $kwargs['angle'];
				$this->_matrix = array(array(cos($o), -sin($o), 0, 0), array(sin($o), cos($o), 0, 0), array(0, 0, 1, 0), array(0, 0, 0, 1));
			}
			if ($kwargs['preset'] == self::PROJECTION) {
				$str = 'PROJECTION preset';
				$fov = 1 / tan(deg2rad($kwargs['fov'] / 2));
				$r = $kwargs['ratio'];
				$n = $kwargs['near'];
				$f = $kwargs['far'];
				$this->_matrix = array(array($fov / $r, 0, 0, 0), array(0, $fov, 0, 0), array(0, 0, -($f+$n)/($f-$n), (2*$n*$f)/($n-$f)), array(0, 0, -1, 0));
			}
		}
		if (self::$verbose == True && array_key_exists('preset', $kwargs))
			print('Matrix '.$str.' instance constructed'.PHP_EOL);
	}

	function __destruct() {
		if (self::$verbose == True)
			print('Matrix Instance destructed'.PHP_EOL);
	}

	function getMatrix() {
		return $this->_matrix;
	}

	function getCoord($x, $y) {
		return $this->_matrix[$x][$y];
	}

	function __toString() {
		return 'M | vtcX | vtcY | vtcZ | vtx0'.PHP_EOL.
			'-----------------------------'.PHP_EOL.
			'x | '.number_format($this->getMatrix()[0][0], 2).' | '.number_format($this->getMatrix()[0][1], 2).' | '.number_format($this->getMatrix()[0][2], 2).' | '.number_format($this->getMatrix()[0][3], 2).PHP_EOL.
			'y | '.number_format($this->getMatrix()[1][0], 2).' | '.number_format($this->getMatrix()[1][1], 2).' | '.number_format($this->getMatrix()[1][2], 2).' | '.number_format($this->getMatrix()[1][3], 2).PHP_EOL.
			'z | '.number_format($this->getMatrix()[2][0], 2).' | '.number_format($this->getMatrix()[2][1], 2).' | '.number_format($this->getMatrix()[2][2], 2).' | '.number_format($this->getMatrix()[2][3], 2).PHP_EOL.
			'w | '.number_format($this->getMatrix()[3][0], 2).' | '.number_format($this->getMatrix()[3][1], 2).' | '.number_format($this->getMatrix()[3][2], 2).' | '.number_format($this->getMatrix()[3][3], 2);
	}

	function mult(Matrix $rhs) {
		$m = clone $this;
		$i = -1;
		while (++$i < 4) {
			$j = -1;
			while (++$j < 4) {
				$sum = 0;
				$k = -1;
				while (++$k < 4)
					$sum += $this->getCoord($i, $k) * $rhs->getCoord($k, $j);
				$m->_matrix[$i][$j] = $sum;
			}
		}
		return $m;
	}

	function transformVertex(Vertex $vtx) {
		$x = $this->getCoord(0, 0) * $vtx->getX() + $this->getCoord(0, 1) * $vtx->getY() + $this->getCoord(0, 2) * $vtx->getZ() + $this->getCoord(0, 3) * $vtx->getW();
		$y = $this->getCoord(1, 0) * $vtx->getX() + $this->getCoord(1, 1) * $vtx->getY() + $this->getCoord(1, 2) * $vtx->getZ() + $this->getCoord(1, 3) * $vtx->getW();
		$z = $this->getCoord(2, 0) * $vtx->getX() + $this->getCoord(2, 1) * $vtx->getY() + $this->getCoord(2, 2) * $vtx->getZ() + $this->getCoord(2, 3) * $vtx->getW();
		$w = $this->getCoord(3, 0) * $vtx->getX() + $this->getCoord(3, 1) * $vtx->getY() + $this->getCoord(3, 2) * $vtx->getZ() + $this->getCoord(3, 3) * $vtx->getW();
		return new Vertex(array('x' => $x, 'y' => $y, 'z' => $z, 'w' => $w));
	}

}

?>