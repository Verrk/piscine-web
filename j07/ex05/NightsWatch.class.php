<?php

class NightsWatch implements IFighter {

	private $_perso;

	public function __construct() {
		$this->_perso = array();
	}

	public function recruit($perso) {
		if (in_array('IFighter', class_implements($perso)))
			array_push($this->_perso, $perso);
	}

	public function fight() {
		foreach ($this->_perso as $p)
			$p->fight();
	}

}

?>