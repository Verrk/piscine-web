<?php

class UnholyFactory {

	private $_f;

	public function __construct() {
		$this->_f = array();
	}

	public function absorb($fighter) {
		if (get_parent_class($fighter) == 'Fighter') {
			if (array_key_exists($fighter->getType(), $this->_f))
				print('(Factory already absorb a fighter of type '.$fighter->getType().')'.PHP_EOL);
			else {
				$this->_f[$fighter->getType()] = get_class($fighter);
				print('(Factory absorb a fighter of type '.$fighter->getType().')'.PHP_EOL);
			}
		}
		else
			print('(Factory can\'t absorb this, it\'s not a fighter)'.PHP_EOL);
	}

	public function fabricate($f) {
		if (array_key_exists($f, $this->_f)) {
			print('(Factory fabricates a fighter of type '.$f.')'.PHP_EOL);
			return new $this->_f[$f];
		}
		else {
			print('(Factory hasn\'t absorbed any fighter of type '.$f.')'.PHP_EOL);
			return NULL;
		}
	}

}

?>