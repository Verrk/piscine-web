#!/usr/bin/php
<?php
if ($argc != 4)
    echo "Incorrect Parameters";
else
{
    $op = trim($argv[2]);
    $num1 = intval(trim($argv[1]));
    $num2 = intval(trim($argv[3]));
    if ($op == "+")
        $res = $num1 + $num2;
    if ($op == "-")
        $res = $num1 - $num2;
    if ($op == "*")
        $res = $num1 * $num2;
    if ($op == "/")
        $res = $num1 / $num2;
    if ($op == "%")
        $res = $num1 % $num2;
    echo $res;
}
?>