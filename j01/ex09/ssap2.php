#!/usr/bin/php
<?php
$i = 0;
foreach ($argv as $elem)
{
    if ($elem != $argv[0])
    {
        $tmp = explode(" ", $elem);
        for ($j = 0; $j < count($tmp); $j++, $i++)
            $array[$i] = $tmp[$j];
    }
}
$i = 0;
$j = 0;
$k = 0;
foreach ($array as $elem)
{
    $c = substr($elem, 0, 1);
    if ($c >= 'A' & $c <= 'Z' | $c >= "a" & $c <= 'z')
    {
        $alpha[$i] = $elem;
        $i++;
    }
    elseif ($c >= '1' & $c <= '9')
    {
        $num[$j] = $elem;
        $j++;
    }
    else
    {
        $other[$k] = $elem;
        $k++;
    }
}
sort($alpha, SORT_STRING | SORT_FLAG_CASE);
sort($num, SORT_STRING | SORT_FLAG_CASE);
sort($other, SORT_STRING | SORT_FLAG_CASE);
foreach ($alpha as $elem)
    echo $elem . "\n";
foreach ($num as $elem)
    echo $elem . "\n";
foreach ($other as $elem)
    echo $elem . "\n";
?>