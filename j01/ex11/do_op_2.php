#!/usr/bin/php
<?php

function epur($s)
{
    $str = trim($s);
    $array = explode(' ', $str);
    $res = "";
    foreach ($array as $elem)
    {
        if (!empty($elem))
            $res = $res.$elem;
    }
    return trim($res);
}

if ($argc != 2)
    echo "Incorrect Parameters";
else
{
    $str = epur($argv[1]);
    $i = 0;
    for ($j = 0; $str[$j] >= '1' & $str[$j] <= '9'; $i++, $j++);
    $num = substr($str, 0, $i);
    if (!is_numeric($num))
    {
        echo "Syntax Error";
        exit(0);
    }
    $num1 = intval($num);
    $op = substr($str, $i, 1);
    $num = substr($str, $i + 1);
    if (!is_numeric($num))
    {
        echo "Syntax Error";
        exit(0);
    }
    $num2 = intval($num);
    if ($op == "+")
        $res = $num1 + $num2;
    elseif ($op == "-")
        $res = $num1 - $num2;
    elseif ($op == "*")
        $res = $num1 * $num2;
    elseif ($op == "/")
        $res = $num1 / $num2;
    elseif ($op == "%")
        $res = $num1 % $num2;
    else
        $res = "Syntax Error";
    echo $res;
}
?>