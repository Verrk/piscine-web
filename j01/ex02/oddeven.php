#!/usr/bin/php
<?php
$fp = fopen('php://stdin', 'r');
while (!feof($fp))
{
    echo "Entrez un nombre: ";
    $in = trim(fgets($fp));
    if (!is_numeric($in))
        echo "'$in' n'est pas un chiffre\n";
    elseif ($in % 2 == 0)
        echo "Le chiffre $in est Pair\n";
    else
        echo "Le chiffre $in est Impair\n";
}
?>