#!/usr/bin/php
<?php
$array = explode(" ", $argv[1]);
if (count($array) == 1)
    echo $array[0];
else
{
    for ($i = 0; $i < count($array) - 1; $i++)
    {
        $tmp = $array[$i];
        $array[$i] = $array[$i + 1];
        $array[$i + 1] = $tmp;
        echo $array[$i] . " ";
    }
    echo $array[count($array) - 1];
}
?>