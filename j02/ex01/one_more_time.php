#!/usr/bin/php
<?php
if ($argc >= 2)
{
    $day = "([Ll]undi|[Mm]ardi|[Mm]ercredi|[Jj]eudi|[Vv]endredi|[Ss]amedi|[Dd]imanche)";
    $numday = "[0-9]{1,2}";
    $month = "([Jj]anvier|[Ff]evrier|[Mm]ars|[Aa]vril|[Mm]ai|[Jj]uin|";
    $month .= "[Jj]uillet|[Aa]out|[Ss]eptembre|[Oo]ctobre|[Nn]ovembre|[Dd]ecember)";
    $year = "[0-9]{4}";
    $time = "[0-9]{2}:[0-9]{2}:[0-9]{2}";
    $date = "/".$day." ".$numday." ".$month." ".$year." ".$time."/";

    if (preg_match($date, $argv[1]))
    {
        $array = explode(' ', $argv[1]);
        $numday = $array[1];
        if (strlen($numday) == 1)
            $numday = "0".$numday;
        if (preg_match("/[Jj]anvier/", $array[2]))
            $month = "01";
        if (preg_match("/[Ff]evrier/", $array[2]))
            $month = "02";
        if (preg_match("/[Mm]ars/", $array[2]))
            $month = "03";
        if (preg_match("/[Aa]vril/", $array[2]))
            $month = "04";
        if (preg_match("/[Mm]ai/", $array[2]))
            $month = "05";
        if (preg_match("/[Jj]uin/", $array[2]))
            $month = "06";
        if (preg_match("/[Jj]uillet/", $array[2]))
            $month = "07";
        if (preg_match("/[Aa]out/", $array[2]))
            $month = "08";
        if (preg_match("/[Ss]eptembre/", $array[2]))
            $month = "09";
        if (preg_match("/[Oo]ctobre/", $array[2]))
            $month = "10";
        if (preg_match("/[Nn]ovembre/", $array[2]))
            $month = "11";
        if (preg_match("/[Dd]ecembre/", $array[2]))
            $month = "12";
        echo strtotime($array[3]."-".$month."-".$numday." ".$array[4]);
    }
    else
        echo "Wrong Format";
}
?>