#!/usr/bin/php
<?php

function	get_data($str)
{
	$c = curl_init($str);
	curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
	$data = curl_exec($c);
	curl_close($c);
	return $data;
}

if (count($argv) > 1)
{
	if (preg_match("/http:\/\/.+/", $argv[1]))
		$rep = substr($argv[1], 7);
	else
		$rep = $argv[1];
	$str = get_data($argv[1]);
	if (preg_match_all("/<img.*src=\"([^\"]+)\".*/", $str, $match) != 0)
	{
		mkdir($rep);
		foreach ($match[1] as $elem)
		{
			$img = get_data($elem);
			$tab = explode('/', $elem);
			$fd = fopen($rep."/".end($tab), "w");
			fwrite($fd, $img);
			fclose($fd);
		}
	}
}

?>