#!/usr/bin/php
<?php
function titleup($matches)
{
    return 'title="'.strtoupper($matches[1]).'"';
}

function aup($matches)
{
    return '<a'.$matches[1].'>'.strtoupper($matches[2]).'<';
}

if (count($argv) < 2)
   exit();
else
{
	$fd = fopen($argv[1], "r");
	while ($line = fgets($fd))
	{
		$line = preg_replace_callback('/title="([\w\s]+)"/', "titleup", $line);
    	$line = preg_replace_callback('/<a([^>]+)>([\w\s]+)</', "aup", $line);
    	echo $line;
	}
}

?>