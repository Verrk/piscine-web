SELECT COUNT(*) AS 'films' FROM film
	   WHERE date_fin_affiche >= '2006-10-30' AND date_debut_affiche <= '2007-07-27'
	   OR ((DAY(date_debut_affiche) <= 24 AND MONTH(date_debut_affiche) <= 12) AND
	   	  (DAY(date_fin_affiche) >= 24 AND MONTH(date_fin_affiche) >= 12));
