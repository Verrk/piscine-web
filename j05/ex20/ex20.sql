SELECT f.id_genre, genre.nom AS 'nom genre', f.id_distrib, distrib.nom AS 'nom distrib', titre AS 'titre film' FROM film f
	   INNER JOIN genre ON genre.id_genre = f.id_genre
	   LEFT OUTER JOIN distrib ON distrib.id_distrib = f.id_distrib
	   WHERE f.id_genre >= 4 AND f.id_genre <= 8

